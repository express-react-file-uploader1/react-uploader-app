import React, { useState, Fragment } from "react";
import Message from "./Message";
import Progress from "./Progress";
import envConfig from "./../config/env.config";
import firebase from "./../config/firebase";
import "./FileUpload.css";
import axios from "axios";

function FileUpload() {
  const [files, setFiles] = useState([]);
  const [filesArray, setFilesArray] = useState([]);
  const [tagsString, setTagsString] = useState("#tags");
  const [isUploaded, setIsUploaded] = useState(false);
  const [isPreview, setIsPreview] = useState(false);
  const [taggedPosts, setTaggedPosts] = useState([]);
  const [message, setMessage] = useState("");
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [uploadNewFiles, setUploadNewFiles] = useState(true);
  const [uploadPercentage, setUploadPercentage] = useState(0);
  const [popularTags] = useState([
    "#car",
    "#cat",
    "#cats",
    "#dog",
    "ichigo",
    "girl",
    "#boy",
    "#animal",
    "#animals",
    "#human",
    "anime",
    "photos",
    "videos",
    "songs",
    "scennery",
  ]);

  const uploadNewFileHandle = () => {
    setUploadNewFiles(true);
    setFiles([]);
    setTagsString("#tags");
    setIsPreview(false);
    setTaggedPosts([]);
    setMessage("");
    setIsUploaded(false);
    console.log("Upload new File");
  };
  const getPhotosByTag = (e) => {
    let tag = e.target.firstChild.data;
    tag = tag.replace("#", "");
    axios
      .get(`${envConfig.apiUrl}/upload/${tag}`)
      .then((res) => {
        const arr = res.data;

        setTaggedPosts([...arr]);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onTagsChange = (e) => {
    setTagsString(e.target.value);
  };

  const onChange = (e) => {
    setFilesArray(() => {
      return [];
    });
    const fileList = e.target.files;
    setFiles([...fileList]);

    for (let i = 0; i < fileList.length; i++) {
      const file = fileList[i];
      const fileReader = new FileReader();
      fileReader.onload = () => {
        setFilesArray((pre) => {
          return [
            ...pre,
            {
              fileName: file.name,
              fileSrc: fileReader.result,
              fileType: file.type,
            },
          ];
        });
        if (i === fileList.length - 1) {
          setIsPreview(true);
        }
      };
      fileReader.readAsDataURL(file);
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (isUploaded) {
      setMessage("Alerady  Uploaded ");
      return;
    }

    if (files.length === 0) {
      setMessage("Select atleast 1 file ");
      return;
    }
    if (tagsString === "#tags") {
      setMessage(`Add some  tags  other than "#tags"`);
      return;
    }
    Promise.all(
      files.map(async (file, i) => {
        const storageRef = firebase.storage().ref(`uploads/${file.name}`);
        await storageRef.put(file);
        const fileUrl = await storageRef.getDownloadURL();
        setFilesArray((pre) => {
          pre[i].fileSrc = fileUrl;
          return pre;
        });
      })
    ).then((res) => {
      const data = {
        files: filesArray,
        tags: tagsString,
      };
      axios
        .post(`${envConfig.apiUrl}/upload`, data, {
          //   headers: {
          //     "Content-Type": "multipart/form-data",
          // },
          onUploadProgress: (progressEvent) => {
            setUploadPercentage(
              parseInt(
                Math.round((progressEvent.loaded * 100) / progressEvent.total)
              )
            );
          },
        })
        .then((res) => {
          setTimeout(() => setUploadPercentage(0), 1000);
          setUploadedFiles(res.data);
          console.log(res.data)
          setIsUploaded(true);
          setUploadNewFiles(false);

          setMessage("File Uploaded");
          setFiles([]);
          setTagsString("#tags");
          setIsPreview(false);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };

 

  return (
    <Fragment>
      {uploadNewFiles ? (
        <div>
          {message ? <Message msg={message} /> : null}

          <form>
            <div className="custom-file mb-4">
              <input
                type="file"
                className="custom-file-input"
                id="customFile"
                onChange={onChange}
                accept="image/* , video/*"
                multiple="true"
              />
              <label className="custom-file-label" htmlFor="customFile">
                Choose file/s
              </label>
            </div>
            <div class="form-group">
              <input
                onChange={(e) => onTagsChange(e)}
                class="form-control"
                id="tags"
                placeholder={tagsString}
              />
            </div>
            <Progress percentage={uploadPercentage} />
          </form>
          <button onClick={onSubmit} className="btn btn-primary btn-block mt-4">
            Upload
          </button>
        </div>
      ) : (
        <button
          type="submit"
          onClick={uploadNewFileHandle}
          className="btn btn-primary btn-block mt-4"
        >
          Upload new files
        </button>
      )}

      {isPreview ? (
        <div className="upload__preview">
          <h4> Preview of Uploads</h4>
          <div className="upload__posts">
            {files.map((file) => {
              return (
                <div className="upload__container">
                  <p>{file.name}</p>
                  {file.type !== "" && file.type.includes("image") ? (
                    <img
                      src={URL.createObjectURL(file)}
                      style={{ width: "100%" }}
                      alt="loading...."
                    />
                  ) : null}

                  {file.type.includes("video") ? (
                    <video
                      controls
                      autoPlay="true"
                      style={{ width: "100%" }}
                      muted
                    >
                      <source
                        src={URL.createObjectURL(file)}
                        type={file.type}
                      />
                    </video>
                  ) : null}
                </div>
              );
            })}
          </div>
        </div>
      ) : null}

      {isUploaded ? (
        <div className="upload__preview">
          <h4> Uploaded Files </h4>
          <div className="upload__posts">
            {uploadedFiles.map((file) => {
              return (
                <div className="upload__container">
                  <p>{file.fileName}</p>
                  {file.fileType !== "" && file.fileType.includes("image") ? (
                    <img
                      src={file.fileSrc}
                      style={{ width: "100%" }}
                      alt="loading...."
                    />
                  ) : null}

                  {file.fileType.includes("video") ? (
                    <video
                      controls
                      autoPlay="true"
                      style={{ width: "100%" }}
                      muted
                    >
                      <source src={file.fileSrc} type={file.fileType} />
                    </video>
                  ) : null}
                  <div>
                    <div>
                      {/* {file.tags.map((tag) => {
                        return (
                          <button
                            style={{ margin: "5px" }}
                            className="btn btn-primary"
                            onClick={(e) => getPhotosByTag(e)}
                          >
                            tag
                          </button>
                        );
                      })} */}
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      ) : null}

      <div className="uploaded__posts">
        <h3>Popular Tags</h3>
        <div>
          {popularTags.map((tag) => {
            return (
              <button
                style={{ margin: "5px" }}
                className="btn btn-primary"
                onClick={(e) => getPhotosByTag(e)}
              >
                {tag}
              </button>
            );
          })}
        </div>
        <hr></hr>
        <div className="uploaded__posts__wrapper">
          {taggedPosts.map((post) => {
            return (
              <div className="posts__container">
                {/* <h5 className="text-center">{post.filename}</h5> */}
                {post.filetype && post.filetype.includes("video") ? (
                  <video controls autoPlay="true" muted="true">
                    <source src={post.filesrc} type={post.filetype} />
                  </video>
                ) : (
                  <img src={post.filesrc} alt="" />
                )}
                 {/* <div>
                      {post.tags.map((tag) => {
                        return (
                          <button
                            style={{ margin: "5px" }}
                            className="btn btn-primary"
                            onClick={(e) => getPhotosByTag(e)}
                          >
                            {tag}
                          </button>
                        );
                      })}
                    </div> */}
              </div>
            );
          })}
        </div>
      </div>
    </Fragment>
  );
}

export default FileUpload;
