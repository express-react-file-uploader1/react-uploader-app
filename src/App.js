import React from "react";
import FileUpload from "./components/FileUpload";
// import './App.css';

const App = () => (
  <div className="container mt-4">
    <h4 className="display-4 text-center mb-4">Upload Your Files here</h4>
    <p className="text-center ">
      <u>
        <i>U can upload images/ gifs/videos & single/multiple at a time</i>
      </u>
    </p>

    <FileUpload />
  </div>
);

export default App;
